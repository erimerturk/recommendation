package com.erim.controller;

import com.erim.CategoryNodeService;
import com.erim.domain.CategoryNode;
import com.erim.test.Application;
import com.erim.test.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;

@Controller
@RequestMapping("/welcome")
public class HelloController {

    @Autowired
    private RecommendationService recommendationService;

    @Autowired
    private CategoryNodeService categoryNodeService;

    @Autowired
    private Application application;


    @RequestMapping(value = "/init", method = RequestMethod.GET)
    public String initTestValues() {

        application.work();
        return "hello";

    }


    @RequestMapping(value = "/ds", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {

        model.addAttribute("message", "Spring 3 MVC Hello World");

        recommendationService.testDS();
        return "hello";

    }

    @RequestMapping(value = "/basic", method = RequestMethod.GET)
    public String createWithBasic(ModelMap model) {

        model.addAttribute("message", "started");

        recommendationService.createCategory();
        return "hello";

    }

    @RequestMapping(value = "/allc", method = RequestMethod.GET)
    public String findAllCategories() {

        Set<CategoryNode> all = categoryNodeService.findAll();

        for(CategoryNode n : all){
            System.out.println(n.getCategoryId());
        }

        return "hello";

    }


    @RequestMapping(value = "/complex", method = RequestMethod.GET)
    public String createWithBatchComplex(ModelMap model) {

        model.addAttribute("message", "Ended");

        recommendationService.fasterOrNot();
        return "hello";

    }

}
