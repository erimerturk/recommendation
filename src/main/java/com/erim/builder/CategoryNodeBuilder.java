package com.erim.builder;

import com.erim.domain.CategoryNode;
import org.apache.commons.lang.math.RandomUtils;

import java.util.HashSet;
import java.util.Set;

public class CategoryNodeBuilder extends BaseBuilder<CategoryNode, CategoryNodeBuilder> {

    private String categoryId = String.valueOf(RandomUtils.nextLong());
    private Set<CategoryNode> childs = new HashSet<CategoryNode>();

    @Override
    protected CategoryNode doBuild() {

        CategoryNode categoryNode = new CategoryNode();
        categoryNode.setCategoryId(categoryId);
        categoryNode.setChildCategories(childs);
        return categoryNode;
    }

    public CategoryNodeBuilder categoryId(String categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public CategoryNodeBuilder child(CategoryNode child) {
        childs.add(child);
        return this;
    }

}
