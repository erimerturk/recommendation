package com.erim.builder;

import com.erim.domain.CategoryNode;
import com.erim.domain.SkuNode;
import org.apache.commons.lang.math.RandomUtils;

public class SkuNodeBuilder extends BaseBuilder<SkuNode, SkuNodeBuilder> {

    private String skuId = String.valueOf(RandomUtils.nextLong());
    private CategoryNode categoryNode;

    @Override
    protected SkuNode doBuild() {

        SkuNode skuNode = new SkuNode();
        skuNode.setSkuId(this.skuId);
        skuNode.setCategory(categoryNode);
        return skuNode;
    }

    public SkuNodeBuilder skuId(Long skuId) {
        this.skuId = String.valueOf(skuId);
        return this;
    }

    public SkuNodeBuilder category(CategoryNode categoryNode) {
        this.categoryNode = categoryNode;
        return this;
    }
}
