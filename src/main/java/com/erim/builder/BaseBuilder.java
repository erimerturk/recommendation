package com.erim.builder;

import com.erim.domain.BaseNode;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.support.Neo4jTemplate;

public abstract class BaseBuilder<T extends BaseNode, B extends BaseBuilder<T, B>> {

    private Long id;

    @SuppressWarnings("unchecked")
    public B id(Long id) {
        this.id = id;
        return (B) this;
    }

    
	@SuppressWarnings("unchecked")

    public Long getId() {
        return id;
    }
    
    @SuppressWarnings("unchecked")
    public T persist(Neo4jTemplate repository) {
        T toPersist = build();
        return (T) repository.save(toPersist);
    }

    public T persist(GraphRepository<T> repository) {
        T toPersist = build();
        return (T) repository.save(toPersist);
    }

    public T build() {
        T baseEntity = doBuild();
        baseEntity.setId(id);
        return baseEntity;
    }

    protected abstract T doBuild();

}
