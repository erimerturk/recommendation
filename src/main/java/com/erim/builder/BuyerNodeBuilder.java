package com.erim.builder;

import com.erim.domain.BuyerNode;
import com.erim.domain.CategoryNode;
import com.erim.domain.GenderNode;
import com.erim.domain.SkuNode;
import org.apache.commons.lang.math.RandomUtils;

import java.util.HashSet;
import java.util.Set;

public class BuyerNodeBuilder extends BaseBuilder<BuyerNode, BuyerNodeBuilder> {

    private String buyerId = String.valueOf(RandomUtils.nextLong());
    private Set<SkuNode> orders = new HashSet<SkuNode>();
    private Set<CategoryNode> orderedCategories = new HashSet<CategoryNode>();
    private GenderNode genderNode;

    @Override
    protected BuyerNode doBuild() {

        BuyerNode buyerNode = new BuyerNode();
        buyerNode.setBuyerId(this.buyerId);
        buyerNode.setOrders(orders);
        buyerNode.setOrderedCategories(orderedCategories);
        buyerNode.setGenderNode(genderNode);
        return buyerNode;
    }

    public BuyerNodeBuilder buyerId(String buyerId) {
        this.buyerId = buyerId;
        return this;
    }

    public BuyerNodeBuilder orders(SkuNode order) {
        this.orders.add(order);
        return this;
    }

    public BuyerNodeBuilder orderedCategories(CategoryNode categoryNode) {
        this.orderedCategories.add(categoryNode);
        return this;
    }

    public BuyerNodeBuilder gender(GenderNode genderNode) {
        this.genderNode = genderNode;
        return this;
    }
}
