package com.erim.builder;

import com.erim.domain.GenderNode;
import com.erim.domain.GenderType;

public class GenderNodeBuilder extends BaseBuilder<GenderNode, GenderNodeBuilder> {

    private GenderType gender = GenderType.FEMALE;

    @Override
    protected GenderNode doBuild() {

        GenderNode node = new GenderNode();
        node.setGenderType(gender);
        return node;
    }

    public GenderNodeBuilder gender(GenderType gender) {
        this.gender = gender;
        return this;
    }

}
