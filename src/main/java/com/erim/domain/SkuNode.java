package com.erim.domain;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class SkuNode extends BaseNode {

    @GraphId
    private Long id;

    @Indexed
    private String skuId;

    @RelatedTo(type = "CATEGORY")
    private CategoryNode category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public CategoryNode getCategory() {
        return category;
    }

    public void setCategory(CategoryNode category) {
        this.category = category;
    }
}
