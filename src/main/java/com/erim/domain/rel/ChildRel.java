package com.erim.domain.rel;

import com.erim.domain.BaseNode;
import com.erim.domain.CategoryNode;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type = "CHILD_CATEGORY")
public class ChildRel extends BaseNode {

    @GraphId Long id;
    @StartNode CategoryNode startThing;
    @EndNode CategoryNode endThing;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CategoryNode getStartThing() {
        return startThing;
    }

    public void setStartThing(CategoryNode startThing) {
        this.startThing = startThing;
    }

    public CategoryNode getEndThing() {
        return endThing;
    }

    public void setEndThing(CategoryNode endThing) {
        this.endThing = endThing;
    }
}
