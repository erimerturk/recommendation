package com.erim.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class CategoryGroupNode extends BaseNode {

    @GraphId
    private Long id;

    @Indexed
    private String categoryGroupId;

    @RelatedTo(type = "CHILD_CATEGORY", direction = Direction.OUTGOING)
    private Set<CategoryNode> childCategories = new HashSet<CategoryNode>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryGroupId() {
        return categoryGroupId;
    }

    public void setCategoryGroupId(String categoryGroupId) {
        this.categoryGroupId = categoryGroupId;
    }

    public Set<CategoryNode> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(Set<CategoryNode> childCategories) {
        this.childCategories = childCategories;
    }
}
