package com.erim.domain;

import org.springframework.data.neo4j.annotation.MapResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

@MapResult
public interface RecommendedSku {

    @ResultColumn("items") SkuNode getItem();

}
