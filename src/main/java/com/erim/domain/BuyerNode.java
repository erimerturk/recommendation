package com.erim.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class BuyerNode extends BaseNode {

    @GraphId
    Long id;

    @Indexed
    private String buyerId;

    @RelatedTo(type = "ORDERED", direction = Direction.BOTH)
    private Set<SkuNode> orders = new HashSet<SkuNode>();

    @RelatedTo(type = "ORDERED_CATEGORIES", direction = Direction.OUTGOING)
    private Set<CategoryNode> orderedCategories = new HashSet<CategoryNode>();

    @RelatedTo(type ="GENDER")
    private GenderNode genderNode;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public Set<SkuNode> getOrders() {
        return orders;
    }

    public void setOrders(Set<SkuNode> orders) {
        this.orders = orders;
    }

    public Set<CategoryNode> getOrderedCategories() {
        return orderedCategories;
    }

    public void setOrderedCategories(Set<CategoryNode> orderedCategories) {
        this.orderedCategories = orderedCategories;
    }

    public GenderNode getGenderNode() {
        return genderNode;
    }

    public void setGenderNode(GenderNode genderNode) {
        this.genderNode = genderNode;
    }
}
