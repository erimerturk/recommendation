package com.erim.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@NodeEntity
public class CategoryNode extends BaseNode {

    @GraphId
    private Long id;

    @Indexed
    private String categoryId;

    @RelatedTo(type = "CHILD_CATEGORY", direction = Direction.OUTGOING)
    private Set<CategoryNode> childCategories = new HashSet<CategoryNode>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Set<CategoryNode> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(Set<CategoryNode> childCategories) {
        this.childCategories = childCategories;
    }

    public Map<String, Object> getExactIndexedProperties() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("categoryId", categoryId);
        return map;
    }
}
