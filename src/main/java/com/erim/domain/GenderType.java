package com.erim.domain;

public enum GenderType {
    MALE, FEMALE
}
