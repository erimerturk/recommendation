package com.erim.domain;

import org.neo4j.graphdb.RelationshipType;

public enum RelationshipTypes implements RelationshipType {

    ORDERED,
    CHILD_CATEGORY,
    ORDERED_CATEGORIES,
    GENDER,
    CATEGORY
}
