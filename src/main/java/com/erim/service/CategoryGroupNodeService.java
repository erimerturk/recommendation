package com.erim.service;

import com.erim.dao.CategoryGroupNodeRepository;
import com.erim.domain.CategoryGroupNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Service
public class CategoryGroupNodeService {

    @Autowired
    private CategoryGroupNodeRepository categoryGroupNodeRepository;

    public Set<CategoryGroupNode> findGroup(){

        EndResult<CategoryGroupNode> all = categoryGroupNodeRepository.findAll();
        Iterator<CategoryGroupNode> iterator = all.iterator();
//        return IteratorUtil.asSet(IteratorUtil.asIterable(iterator));
        return new HashSet<CategoryGroupNode>();

    }

    public CategoryGroupNode findByGroupId(String groupId){


        return categoryGroupNodeRepository.findByCategoryGroupId(groupId);

    }


}
