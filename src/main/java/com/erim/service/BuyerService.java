package com.erim.service;

import com.erim.dao.BuyerNodeRepository;
import com.erim.domain.BuyerNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class BuyerService {

    @Autowired private BuyerNodeRepository buyerNodeRepository;

    public Set<BuyerNode> buyers(){
        EndResult<BuyerNode> all = buyerNodeRepository.findAll();
//        return IteratorUtil.asSet(IteratorUtil.asIterable(all.iterator()));
        return new HashSet<BuyerNode>();
    }

}
