package com.erim;

import com.erim.builder.CategoryGroupNodeBuilder;
import com.erim.builder.CategoryNodeBuilder;
import com.erim.dao.CategoryGroupNodeRepository;
import com.erim.dao.CategoryNodeRepository;
import com.erim.domain.CategoryGroupNode;
import com.erim.domain.CategoryNode;
import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.Set;

@Service
@Transactional
public class CategoryNodeService {

    @Autowired
    private CategoryGroupNodeRepository categoryGroupNodeRepository;

    @Autowired
    private CategoryNodeRepository categoryNodeRepository;

    @Autowired private GraphDatabaseService graphDatabaseService;

    @Transactional
    public void create(String categoryId, String categoryGroup) {

        CategoryNode categoryNode = new CategoryNodeBuilder().categoryId(categoryId).persist(categoryNodeRepository);

        CategoryGroupNode fromNEO = categoryGroupNodeRepository.findByCategoryGroupId(categoryGroup);

        if (fromNEO == null) {
            new CategoryGroupNodeBuilder().categoryGroupId(categoryGroup).child(categoryNode).persist(categoryGroupNodeRepository);
        } else {
            fromNEO.getChildCategories().add(categoryNode);
            categoryGroupNodeRepository.save(fromNEO);
        }

    }

    public Set<CategoryNode> findAll(){

        EndResult<CategoryNode> all = categoryNodeRepository.findAll();
        Iterator<CategoryNode> iterator = all.iterator();
//        return IteratorUtil.asSet(IteratorUtil.asIterable(iterator));
        return null;

    }

    @Transactional
    public void create(String categoryId) {

        CategoryNode categoryNode = new CategoryNodeBuilder().categoryId(categoryId).persist(categoryNodeRepository);

    }


    @Transactional
    public void save(CategoryNode p , CategoryNode c) {

        p.getChildCategories().add(categoryNodeRepository.findByCategoryId(c.getCategoryId()));
        categoryNodeRepository.save(p);

    }

    @Transactional
    public void createWithParent(String categoryId, String parentId) {

        CategoryNode parentP = categoryNodeRepository.findByCategoryId(parentId);

        CategoryNode child = categoryNodeRepository.findByCategoryId(categoryId);

        if (child == null) {

            child = new CategoryNodeBuilder().categoryId(categoryId).persist(categoryNodeRepository);
        }

        if (parentP == null) {

            new CategoryNodeBuilder().categoryId(parentId).child(child).persist(categoryNodeRepository);

        } else {

            if (!parentP.getChildCategories().contains(child)) {

                parentP.getChildCategories().add(child);

//                neo4jTemplate.createRelationshipBetween(parentP, child, ChildRel.class, String.valueOf(RelationshipTypes.CHILD_CATEGORY), false);

                categoryNodeRepository.save(parentP);
            }
        }


    }
}
