package com.erim.test;

import com.erim.CategoryNodeService;
import com.erim.dao.BuyerNodeRepository;
import com.erim.dao.SkuNodeRepository;
import com.erim.domain.BuyerNode;
import com.erim.domain.RecommendedSku;
import com.erim.domain.SkuNode;
import com.erim.util.Clock;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class RecommendationService {

    @Autowired
    private SkuNodeRepository skuNodeRepository;

    @Autowired
    private BuyerNodeRepository buyerNodeRepository;

//    @Autowired
    private DataSource dataSource;

    @Autowired
    private CategoryNodeService categoryNodeService;

    public List<SkuNode> recommendFor(String skuId, String buyerId){


        SkuNode view = skuNodeRepository.findBySkuId(skuId);
        BuyerNode viewer = buyerNodeRepository.findByBuyerId(buyerId);

        List<RecommendedSku> recommendedSkus = skuNodeRepository.recommendSkuFor(view, viewer);

        ArrayList<SkuNode> recommended = new ArrayList<SkuNode>();

        for(RecommendedSku recommendedSku : recommendedSkus){
            recommended.add(recommendedSku.getItem());
        }

        return recommended;
    }

    public void testDS(){
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement("select id, parent_id from Category where parent_id is not null");
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                categoryNodeService.createWithParent(resultSet.getString("id"), resultSet.getString("parent_id"));
            }


            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void createCategory(){

        System.out.println("Start date : " +  Clock.getTime());

        for(long i = 0; i < 10000; i++){
           categoryNodeService.create(RandomStringUtils.randomAlphabetic(15));
        }
        System.out.println("End date : " +  Clock.getTime());

    }

    public void fasterOrNot(){

        System.out.println("Start date : " +  Clock.getTime());

        for(long i = 0; i < 10000; i++){


            categoryNodeService.createWithParent(String.valueOf(RandomUtils.nextInt(3000)), String.valueOf(RandomUtils.nextInt(3000)));

        }
        System.out.println("End date : " +  Clock.getTime());

    }

}
