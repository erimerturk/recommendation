package com.erim.test;

import com.erim.builder.BuyerNodeBuilder;
import com.erim.builder.CategoryGroupNodeBuilder;
import com.erim.builder.CategoryNodeBuilder;
import com.erim.builder.GenderNodeBuilder;
import com.erim.builder.SkuNodeBuilder;
import com.erim.dao.BuyerNodeRepository;
import com.erim.dao.CategoryGroupNodeRepository;
import com.erim.dao.CategoryNodeRepository;
import com.erim.dao.GenderNodeRepository;
import com.erim.dao.SkuNodeRepository;
import com.erim.domain.BuyerNode;
import com.erim.domain.CategoryGroupNode;
import com.erim.domain.CategoryNode;
import com.erim.domain.GenderNode;
import com.erim.domain.GenderType;
import com.erim.domain.SkuNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class Application {

    @Autowired
    private BuyerNodeRepository buyerRepo;

    @Autowired
    private SkuNodeRepository skuRepo;

    @Autowired
    private CategoryNodeRepository categoryNodeRepository;

    @Autowired
    private CategoryGroupNodeRepository categoryGroupNodeRepository;

    @Autowired
    private GenderNodeRepository genderRepo;

    @Transactional
    public void work() {

        GenderNode female = new GenderNodeBuilder().gender(GenderType.FEMALE).persist(genderRepo);
        GenderNode male = new GenderNodeBuilder().gender(GenderType.MALE).persist(genderRepo);

        CategoryNode child111 = new CategoryNodeBuilder().persist(categoryNodeRepository);
        CategoryNode child112 = new CategoryNodeBuilder().persist(categoryNodeRepository);

        CategoryNode child11 = new CategoryNodeBuilder().child(child111).child(child112).persist(categoryNodeRepository);

        CategoryNode child121 = new CategoryNodeBuilder().persist(categoryNodeRepository);
        CategoryNode child12 = new CategoryNodeBuilder().child(child121).persist(categoryNodeRepository);

        CategoryNode child1 = new CategoryNodeBuilder().child(child11).child(child12).persist(categoryNodeRepository);

        CategoryNode child211 = new CategoryNodeBuilder().persist(categoryNodeRepository);
        CategoryNode child21 = new CategoryNodeBuilder().child(child211).persist(categoryNodeRepository);
        CategoryNode child2 = new CategoryNodeBuilder().child(child21).persist(categoryNodeRepository);


        CategoryNode parent = new CategoryNodeBuilder().child(child1).child(child2).persist(categoryNodeRepository);

        CategoryNode achild111 = new CategoryNodeBuilder().persist(categoryNodeRepository);
        CategoryNode achild112 = new CategoryNodeBuilder().persist(categoryNodeRepository);

        CategoryNode achild11 = new CategoryNodeBuilder().child(achild111).child(achild112).persist(categoryNodeRepository);

        CategoryNode achild121 = new CategoryNodeBuilder().persist(categoryNodeRepository);
        CategoryNode achild12 = new CategoryNodeBuilder().child(achild121).persist(categoryNodeRepository);

        CategoryNode achild1 = new CategoryNodeBuilder().child(achild11).child(achild12).persist(categoryNodeRepository);


        CategoryNode achild211 = new CategoryNodeBuilder().persist(categoryNodeRepository);
        CategoryNode achild21 = new CategoryNodeBuilder().child(achild211).persist(categoryNodeRepository);
        CategoryNode achild2 = new CategoryNodeBuilder().child(achild21).persist(categoryNodeRepository);

        CategoryNode parent2 = new CategoryNodeBuilder().child(achild1).child(achild2).persist(categoryNodeRepository);

        SkuNode sku1 = new SkuNodeBuilder().category(child111).persist(skuRepo);
        SkuNode sku2 = new SkuNodeBuilder().category(achild111).persist(skuRepo);

        BuyerNode buyer1 = new BuyerNodeBuilder().gender(female).orderedCategories(child111).orderedCategories(achild111).orders(sku1).orders(sku2).persist(buyerRepo);

        CategoryGroupNode electronic = new CategoryGroupNodeBuilder().child(parent).child(parent2).persist(categoryGroupNodeRepository);


    }

}
