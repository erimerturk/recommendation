package com.erim.dao;

import com.erim.domain.BuyerNode;
import com.erim.domain.RecommendedSku;
import com.erim.domain.SkuNode;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

public interface SkuNodeRepository extends GraphRepository<SkuNode> {


    SkuNode findBySkuId (String skuId);

    @Query("start item= node({0}), currentBuyer=node({1}) " +
            "match item-[:ORDERED]-person-[:ORDERED]-items, currentBuyer-[r2?:GENDER]-()-[r1?:GENDER]-person " +
            "where not(currentBuyer-[:ORDERED]-items)" +
            "return items, count(items) order by count(items) desc limit 10")
    public List<RecommendedSku> recommendSkuFor(SkuNode skuNode, BuyerNode buyerNode);


    @Query("start item= node({0}), currentBuyer=node({1}) " +
            "match item-[:ORDERED]-person-[:ORDERED]-items, currentBuyer-[:GENDER]-()-[:GENDER]-person " +
            "where not(currentBuyer-[:ORDERED]-items)" +
            "return items, count(items) order by count(items) desc limit 10")
    public List<RecommendedSku> recommendSkuForWithGender(SkuNode skuNode2, BuyerNode buyerNode);
}
