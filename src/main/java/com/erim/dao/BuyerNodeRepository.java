package com.erim.dao;

import com.erim.domain.BuyerNode;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

public interface BuyerNodeRepository  extends GraphRepository<BuyerNode> {

    BuyerNode findByBuyerId (String buyerId);

    @Query("start me= node({0}) match me-[:ORDERED]-item-[:ORDERED]-others return others;")
    List<BuyerNode> findLikeMePeople(BuyerNode buyerNode);

}
