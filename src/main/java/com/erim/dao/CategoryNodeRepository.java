package com.erim.dao;

import com.erim.domain.CategoryNode;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

public interface CategoryNodeRepository extends GraphRepository<CategoryNode> {

    CategoryNode findByCategoryId(String categoryId);


    @Query("start parent= node({0}) match parent-[:CHILD_CATEGORY]->childs return childs;")
    List<CategoryNode> findByParent(CategoryNode parent);

    @Query("start parent= node({0}) match parent-[:CHILD_CATEGORY*]->childs return childs;")
    List<CategoryNode> findByParentAllChilds(CategoryNode parent);
}
