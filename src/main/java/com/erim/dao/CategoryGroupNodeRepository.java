package com.erim.dao;

import com.erim.domain.CategoryGroupNode;
import org.springframework.data.neo4j.repository.GraphRepository;

public interface CategoryGroupNodeRepository extends GraphRepository<CategoryGroupNode> {
    public CategoryGroupNode findByCategoryGroupId(String id);
}
