package com.erim.dao;

import com.erim.domain.GenderNode;
import com.erim.domain.GenderType;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

public interface GenderNodeRepository extends GraphRepository<GenderNode> {

    @Query("start")
    public GenderNode findByGenderType(GenderType genderType);
}
