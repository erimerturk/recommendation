package com.erim;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.data.neo4j.support.node.Neo4jHelper;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(locations = "classpath:/app-context-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class BaseIntegration {

    @Autowired
    protected Neo4jTemplate neo4jTemplate;

    public Neo4jTemplate getNeo4jTemplate() {
        return neo4jTemplate;
    }

    @Rollback(false)
    @BeforeTransaction
    public void cleanUpGraph() {
        Neo4jHelper.cleanDb(getNeo4jTemplate());
    }
}
