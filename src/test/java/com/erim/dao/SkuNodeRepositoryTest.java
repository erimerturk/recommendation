package com.erim.dao;

import com.erim.BaseIntegration;
import com.erim.builder.BuyerNodeBuilder;
import com.erim.builder.GenderNodeBuilder;
import com.erim.builder.SkuNodeBuilder;
import com.erim.domain.BuyerNode;
import com.erim.domain.GenderNode;
import com.erim.domain.GenderType;
import com.erim.domain.RecommendedSku;
import com.erim.domain.SkuNode;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.not;

public class SkuNodeRepositoryTest extends BaseIntegration {

    @Autowired
    private SkuNodeRepository skuNodeRepository;

    @Test
    public void shouldCreateSku(){

        SkuNode skuNode = new SkuNodeBuilder().skuId(1L).persist(getNeo4jTemplate());

        assertThat(skuNodeRepository.findBySkuId("1"), equalTo(skuNode));

    }



    @Test
    public void shouldRecommendOneProduct(){


        // buyer 1 ordered sku1 and view sku2
        // buyer 2 ordered sku1, sku2, sku3
        //should recommend 1 and 3
        // 1 elenmeli mi ?

        SkuNode skuNode1 = new SkuNodeBuilder().skuId(1L).persist(getNeo4jTemplate());
        SkuNode skuNode2 = new SkuNodeBuilder().skuId(2L).persist(getNeo4jTemplate());
        SkuNode skuNode3 = new SkuNodeBuilder().skuId(3L).persist(getNeo4jTemplate());


        BuyerNode buyerNode = new BuyerNodeBuilder().orders(skuNode1).persist(getNeo4jTemplate());
        BuyerNode buyerNode2 = new BuyerNodeBuilder().orders(skuNode1).orders(skuNode2).orders(skuNode3).persist(getNeo4jTemplate());

        List<RecommendedSku> fromNeo = skuNodeRepository.recommendSkuFor(skuNode2, buyerNode);

        ArrayList<SkuNode> recommended = new ArrayList<SkuNode>();

        for(RecommendedSku recommendedSku : fromNeo){
            recommended.add(recommendedSku.getItem());
        }

        assertThat(recommended, hasItems(skuNode3));
        assertThat(recommended, not(hasItems(skuNode1, skuNode2)));

    }

    @Test
    public void shouldRecommendZeroProductWhenAlreadyOrderedAll(){


        // buyer 1 ordered sku1 and view sku2
        // buyer 2 ordered sku1, sku2, sku3
        //should recommend 1 and 3
        // 1 elenmeli mi ?

        SkuNode skuNode1 = new SkuNodeBuilder().skuId(1L).persist(getNeo4jTemplate());
        SkuNode skuNode2 = new SkuNodeBuilder().skuId(2L).persist(getNeo4jTemplate());
        SkuNode skuNode3 = new SkuNodeBuilder().skuId(3L).persist(getNeo4jTemplate());


        BuyerNode buyerNode = new BuyerNodeBuilder().orders(skuNode1).orders(skuNode3).persist(getNeo4jTemplate());
        BuyerNode buyerNode2 = new BuyerNodeBuilder().orders(skuNode1).orders(skuNode2).orders(skuNode3).persist(getNeo4jTemplate());

        List<RecommendedSku> fromNeo = skuNodeRepository.recommendSkuFor(skuNode2, buyerNode);

        ArrayList<SkuNode> recommended = new ArrayList<SkuNode>();

        for(RecommendedSku recommendedSku : fromNeo){
            recommended.add(recommendedSku.getItem());
        }

        assertThat(recommended, not(hasItems(skuNode1, skuNode2, skuNode3)));

    }

    @Test
    public void shouldRecommendOneProductForSameGender(){


        // buyer 1 ordered sku1 and view sku2
        // buyer 2 ordered sku1, sku2, sku3
        //should recommend 1 and 3
        // 1 elenmeli mi ?

        GenderNode male = new GenderNodeBuilder().gender(GenderType.MALE).persist(getNeo4jTemplate());
        GenderNode female = new GenderNodeBuilder().gender(GenderType.FEMALE).persist(getNeo4jTemplate());

        SkuNode skuNode1 = new SkuNodeBuilder().skuId(1L).persist(getNeo4jTemplate());
        SkuNode skuNode2 = new SkuNodeBuilder().skuId(2L).persist(getNeo4jTemplate());
        SkuNode skuNode3 = new SkuNodeBuilder().skuId(3L).persist(getNeo4jTemplate());
        SkuNode skuNode4 = new SkuNodeBuilder().persist(getNeo4jTemplate());

        BuyerNode currentBuyer = new BuyerNodeBuilder().gender(male).orders(skuNode1).persist(getNeo4jTemplate());
        BuyerNode buyerNode2 = new BuyerNodeBuilder().gender(female).orders(skuNode1).orders(skuNode2).orders(skuNode3).persist(getNeo4jTemplate());
        BuyerNode buyerNode3 = new BuyerNodeBuilder().gender(male).orders(skuNode4).orders(skuNode2).persist(getNeo4jTemplate());

        List<RecommendedSku> fromNeo = skuNodeRepository.recommendSkuForWithGender(skuNode2, currentBuyer);

        ArrayList<SkuNode> recommended = new ArrayList<SkuNode>();

        for(RecommendedSku recommendedSku : fromNeo){
            recommended.add(recommendedSku.getItem());
        }

        assertThat(recommended, hasItems(skuNode4));
        assertThat(recommended, not(hasItems(skuNode1, skuNode2, skuNode3)));

    }

    @Test
    public void shouldWorkOptionalRelationShip(){


        // buyer 1 ordered sku1 and view sku2
        // buyer 2 ordered sku1, sku2, sku3
        //should recommend 1 and 3
        // 1 elenmeli mi ?

        GenderNode male = new GenderNodeBuilder().gender(GenderType.MALE).persist(getNeo4jTemplate());
        GenderNode female = new GenderNodeBuilder().gender(GenderType.FEMALE).persist(getNeo4jTemplate());

        SkuNode skuNode1 = new SkuNodeBuilder().skuId(1L).persist(getNeo4jTemplate());
        SkuNode skuNode2 = new SkuNodeBuilder().skuId(2L).persist(getNeo4jTemplate());
        SkuNode skuNode3 = new SkuNodeBuilder().skuId(3L).persist(getNeo4jTemplate());
        SkuNode skuNode4 = new SkuNodeBuilder().persist(getNeo4jTemplate());

        BuyerNode currentBuyer = new BuyerNodeBuilder().gender(male).orders(skuNode1).persist(getNeo4jTemplate());
        BuyerNode buyerNode2 = new BuyerNodeBuilder().gender(female).orders(skuNode1).orders(skuNode2).orders(skuNode3).persist(getNeo4jTemplate());
        BuyerNode buyerNode3 = new BuyerNodeBuilder().gender(male).orders(skuNode4).orders(skuNode2).persist(getNeo4jTemplate());

        List<RecommendedSku> fromNeo = skuNodeRepository.recommendSkuFor(skuNode2, currentBuyer);

        ArrayList<SkuNode> recommended = new ArrayList<SkuNode>();

        for(RecommendedSku recommendedSku : fromNeo){
            recommended.add(recommendedSku.getItem());
        }

        assertThat(recommended, hasItems(skuNode4));
        assertThat(recommended, not(hasItems(skuNode1, skuNode2, skuNode3)));

    }


}
