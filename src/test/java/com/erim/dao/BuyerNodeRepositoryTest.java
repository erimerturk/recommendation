package com.erim.dao;

import com.erim.BaseIntegration;
import com.erim.builder.BuyerNodeBuilder;
import com.erim.builder.SkuNodeBuilder;
import com.erim.domain.BuyerNode;
import com.erim.domain.SkuNode;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class BuyerNodeRepositoryTest extends BaseIntegration {

    @Autowired
    private BuyerNodeRepository buyerNodeRepository;

    @Test
    public void shouldCreateBuyerNode() {

        BuyerNode buyerNode = new BuyerNodeBuilder().buyerId("1").persist(getNeo4jTemplate());

        assertThat(buyerNodeRepository.findByBuyerId("1"), equalTo(buyerNode));

    }

    @Test
    public void shouldFindLikeMePeople() {


        SkuNode skuNode = new SkuNodeBuilder().skuId(1L).persist(getNeo4jTemplate());

        BuyerNode buyerNode1 = new BuyerNodeBuilder().orders(skuNode).buyerId("1").persist(getNeo4jTemplate());
        BuyerNode buyerNode2 = new BuyerNodeBuilder().orders(skuNode).buyerId("2").persist(getNeo4jTemplate());
        BuyerNode buyerNode3 = new BuyerNodeBuilder().buyerId("3").persist(getNeo4jTemplate());



        List<BuyerNode> likeMePeople = buyerNodeRepository.findLikeMePeople(buyerNode1);

        assertThat(likeMePeople, hasItem(buyerNode2));
        assertThat(likeMePeople, not(hasItems(buyerNode1, buyerNode3)));

    }


    @Test
    public void shouldFindWithOrders() {

        SkuNode skuNode1 = new SkuNodeBuilder().skuId(1L).persist(getNeo4jTemplate());
        SkuNode skuNode2 = new SkuNodeBuilder().skuId(2L).persist(getNeo4jTemplate());

        BuyerNode buyerNode1 = new BuyerNodeBuilder().orders(skuNode1).orders(skuNode2).buyerId("1").persist(getNeo4jTemplate());

        BuyerNode fromNeo = buyerNodeRepository.findByBuyerId(buyerNode1.getBuyerId());

        assertThat(fromNeo.getOrders(), hasItems(skuNode1, skuNode2));

    }
}
