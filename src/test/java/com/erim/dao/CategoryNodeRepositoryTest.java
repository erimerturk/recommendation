package com.erim.dao;

import com.erim.BaseIntegration;
import com.erim.builder.CategoryNodeBuilder;
import com.erim.domain.CategoryNode;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CategoryNodeRepositoryTest extends BaseIntegration {

    @Autowired
    private CategoryNodeRepository categoryNodeRepository;

    @Test
    public void shouldCreateCategoryNodeWithoutChildAndParent() {

        CategoryNode categoryNode = new CategoryNodeBuilder().persist(getNeo4jTemplate());

        CategoryNode fromNeo = categoryNodeRepository.findByCategoryId(categoryNode.getCategoryId());

        assertThat(categoryNode, equalTo(fromNeo));

    }

    @Test
    public void shouldCreateCategoryNodeWithoutChild() {

        CategoryNode child11 = new CategoryNodeBuilder().persist(getNeo4jTemplate());
        CategoryNode child1 = new CategoryNodeBuilder().child(child11).persist(getNeo4jTemplate());

        CategoryNode child2 = new CategoryNodeBuilder().persist(getNeo4jTemplate());

        CategoryNode parent = new CategoryNodeBuilder().child(child1).child(child2).persist(getNeo4jTemplate());

        List<CategoryNode> byParent = categoryNodeRepository.findByParent(parent);
        assertThat(byParent, hasItems(child1, child2));

    }

    @Test
    public void shouldFindAllChildsByParent() {

        CategoryNode child11 = new CategoryNodeBuilder().persist(getNeo4jTemplate());
        CategoryNode child1 = new CategoryNodeBuilder().child(child11).persist(getNeo4jTemplate());

        CategoryNode child2 = new CategoryNodeBuilder().persist(getNeo4jTemplate());

        CategoryNode parent = new CategoryNodeBuilder().child(child1).child(child2).persist(getNeo4jTemplate());

        List<CategoryNode> byParent = categoryNodeRepository.findByParentAllChilds(parent);
        assertThat(byParent, hasItems(child1, child2, child11));

    }
}
