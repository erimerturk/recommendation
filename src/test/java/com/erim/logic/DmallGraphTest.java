package com.erim.logic;

import com.erim.BaseIntegration;
import com.erim.builder.BuyerNodeBuilder;
import com.erim.builder.CategoryGroupNodeBuilder;
import com.erim.builder.CategoryNodeBuilder;
import com.erim.builder.SkuNodeBuilder;
import com.erim.dao.CategoryGroupNodeRepository;
import com.erim.domain.CategoryGroupNode;
import com.erim.domain.CategoryNode;
import com.erim.domain.SkuNode;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertNotNull;

public class DmallGraphTest extends BaseIntegration {

    @Autowired
    private CategoryGroupNodeRepository categoryGroupNodeRepository;

    @Test
    public void should() {
        CategoryNode child111 = new CategoryNodeBuilder().persist(getNeo4jTemplate());
        CategoryNode child112 = new CategoryNodeBuilder().persist(getNeo4jTemplate());

        CategoryNode child11 = new CategoryNodeBuilder().child(child111).child(child112).persist(getNeo4jTemplate());

        CategoryNode child121 = new CategoryNodeBuilder().persist(getNeo4jTemplate());
        CategoryNode child12 = new CategoryNodeBuilder().child(child121).persist(getNeo4jTemplate());

        CategoryNode child1 = new CategoryNodeBuilder().child(child11).child(child12).persist(getNeo4jTemplate());

        CategoryNode child211 = new CategoryNodeBuilder().persist(getNeo4jTemplate());
        CategoryNode child21 = new CategoryNodeBuilder().child(child211).persist(getNeo4jTemplate());
        CategoryNode child2 = new CategoryNodeBuilder().child(child21).persist(getNeo4jTemplate());


        CategoryNode parent = new CategoryNodeBuilder().child(child1).child(child2).persist(getNeo4jTemplate());

        CategoryNode achild111 = new CategoryNodeBuilder().persist(getNeo4jTemplate());
        CategoryNode achild112 = new CategoryNodeBuilder().persist(getNeo4jTemplate());

        CategoryNode achild11 = new CategoryNodeBuilder().child(achild111).child(achild112).persist(getNeo4jTemplate());

        CategoryNode achild121 = new CategoryNodeBuilder().persist(getNeo4jTemplate());
        CategoryNode achild12 = new CategoryNodeBuilder().child(achild121).persist(getNeo4jTemplate());

        CategoryNode achild1 = new CategoryNodeBuilder().child(achild11).child(achild12).persist(getNeo4jTemplate());


        CategoryNode achild211 = new CategoryNodeBuilder().persist(getNeo4jTemplate());
        CategoryNode achild21 = new CategoryNodeBuilder().child(achild211).persist(getNeo4jTemplate());
        CategoryNode achild2 = new CategoryNodeBuilder().child(achild21).persist(getNeo4jTemplate());

        CategoryNode parent2 = new CategoryNodeBuilder().child(achild1).child(achild2).persist(getNeo4jTemplate());

        SkuNode sku1 = new SkuNodeBuilder().category(child111).persist(getNeo4jTemplate());
        SkuNode sku2 = new SkuNodeBuilder().category(achild111).persist(getNeo4jTemplate());

        BuyerNodeBuilder buyer1 = new BuyerNodeBuilder().orderedCategories(child111).orderedCategories(achild111).orders(sku1).orders(sku2);

        CategoryGroupNode electronic = new CategoryGroupNodeBuilder().child(parent).child(parent2).persist(getNeo4jTemplate());

        CategoryGroupNode fromNeo = categoryGroupNodeRepository.findByCategoryGroupId(electronic.getCategoryGroupId());

        assertNotNull(fromNeo);
        assertThat(fromNeo.getChildCategories(), hasItems(parent, parent2));
        CategoryNode next = fromNeo.getChildCategories().iterator().next();
        if(next.equals(parent)){
            assertThat(next.getChildCategories(), hasItems(child1, child2));
        }

    }

}
